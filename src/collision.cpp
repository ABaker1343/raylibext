#include "../include/collision.hpp"

CollisionBox2D::CollisionBox2D() {}

CollisionBox2D::CollisionBox2D(const Vector2 bl, const Vector2 br, const Vector2 tl, const Vector2 tr) :
    bottom_left(bl), bottom_right(br), top_left(tl), top_right(tr) {}

CollisionBox2D::CollisionBox2D(const Rectangle rec) :
    bottom_left(Vector2{rec.x, rec.y}), 
    bottom_right(Vector2{rec.x + rec.width, rec.y}),
    top_left(Vector2{rec.x, rec.y + rec.height}),
    top_right(Vector2{rec.x + rec.width, rec.y + rec.height}) {}

Vector2 Vector2Midpoint(const Vector2 v1, const Vector2 v2)
{
    return {(v1.x + v2.x) * 0.5f, (v1.y + v2.y) * 0.5f};
}

void RotateCollisionBox(CollisionBox2D& box, float degrees, CollisionBoxRotationPoint origin)
{
    Vector2 rotation_point;

    switch(origin)
    {
        case BOTTOM_CENTER:
        {
            rotation_point = Vector2Midpoint(box.bottom_left, box.bottom_right);
        }
        case TOP_CENTER:
        {
            rotation_point = Vector2Midpoint(box.top_left, box.top_right);
        }
    }

    box.bottom_left = Vector2Rotate(box.bottom_left - rotation_point, degrees * DEG2RAD) + rotation_point;
    box.bottom_right = Vector2Rotate(box.bottom_right - rotation_point, degrees * DEG2RAD) + rotation_point;
    box.top_left = Vector2Rotate(box.top_left - rotation_point, degrees * DEG2RAD) + rotation_point;
    box.top_right = Vector2Rotate(box.top_right - rotation_point, degrees * DEG2RAD) + rotation_point;
}

std::optional<Vector2> CheckCollisionLineRec(const Vector2 p1, const Vector2 p2, const Rectangle rec)
{
    Vector2 rec_lines[4][2] = {
        // top
        {Vector2{rec.x, rec.y}, Vector2{rec.x + rec.width, rec.y}},
        // left
        {Vector2{rec.x, rec.y}, Vector2{rec.x, rec.y + rec.height}},
        // right
        {Vector2{rec.x + rec.width, rec.y}, Vector2{rec.x + rec.width, rec.y + rec.height}},
        // bottom
        {Vector2{rec.x, rec.y + rec.height}, Vector2{rec.x + rec.width, rec.y + rec.height}},
    };

    for (const auto& line : rec_lines)
    {
        Vector2 collision_point;

        if ( CheckCollisionLines(
                    p1, p2, 
                    line[0], line[1],
                    &collision_point
                    ))
        {
            return collision_point;
        }
    }

    return std::optional<Vector2>(); // empty optional
}

bool CheckCollision(const CollisionBox2D& box, const Rectangle& rec)
{
    std::vector<Vector2> axies;
    axies.reserve(4);
    // get all normals of the boxes
    axies.push_back(Vector2Normalize(Vector2Subtract(box.top_left, box.bottom_left)));
    axies.push_back(Vector2Normalize(Vector2Subtract(box.top_right, box.top_left)));
    // get the normals for the rectangle (always aligned with up and right)
    axies.push_back({0, 1});
    axies.push_back({1, 0});

    // for each axis project the points of the two boxes to that axis
    // check if they collide on that 1d plane
    // if any plane has no collision then they are separated

    for (const auto& axis : axies)
    {
        std::tuple<float, float> box_points = GetMinAndMaxProjectedPoints(box, axis);
        std::tuple<float, float> rec_points = GetMinAndMaxProjectedPoints(rec, axis);

        // if not overlapping return false
        if ( ! (
            std::get<1>(rec_points) > std::get<0>(box_points) &&
            std::get<1>(box_points) > std::get<0>(rec_points)
        )) return false;
    }

    // if they are touching on all axis then they are colliding
    return true;
}

bool CheckCollision(const CollisionBox2D& box1, const CollisionBox2D& box2)
{
    std::vector<Vector2> axies;
    std::vector<Vector2> separating_axis;
    axies.reserve(4);

    // get all normals of the boxes
    axies.push_back(Vector2Normalize(Vector2Subtract(box1.top_left, box1.bottom_left)));
    axies.push_back(Vector2Normalize(Vector2Subtract(box1.top_right, box1.top_left)));
    axies.push_back(Vector2Normalize(Vector2Subtract(box2.top_left, box2.bottom_left)));
    axies.push_back(Vector2Normalize(Vector2Subtract(box2.top_right, box2.top_left)));

    // for each axis project the points of the two boxes to that axis
    // check if they collide on that 1d plane
    // if any plane has no collision then they are separated

    for (const auto& axis : axies)
    {
        std::tuple<float, float> box1_points = GetMinAndMaxProjectedPoints(box1, axis);
        std::tuple<float, float> box2_points = GetMinAndMaxProjectedPoints(box2, axis);

        // if not overlapping return false
        if ( ! (
            std::get<1>(box1_points) > std::get<0>(box2_points) &&
            std::get<1>(box2_points) > std::get<0>(box1_points)
        )) return false;
    }

    // if all axis are overlapping then they are colliding
    return true;
}

std::vector<Vector2> GetSeparatingAxis(const CollisionBox2D& box, const Rectangle& rec)
{
    std::vector<Vector2> axies;
    std::vector<Vector2> separating_axis;
    axies.reserve(4);
    // get all normals of the boxes
    axies.push_back(Vector2Normalize(Vector2Subtract(box.top_left, box.bottom_left)));
    axies.push_back(Vector2Normalize(Vector2Subtract(box.top_right, box.top_left)));
    // get the normals for the rectangle (always aligned with up and right)
    axies.push_back({0, 1});
    axies.push_back({1, 0});

    // for each axis project the points of the two boxes to that axis
    // check if they collide on that 1d plane
    // if any plane has no collision then they are separated

    for (const auto& axis : axies)
    {
        std::tuple<float, float> box_points = GetMinAndMaxProjectedPoints(box, axis);
        std::tuple<float, float> rec_points = GetMinAndMaxProjectedPoints(rec, axis);

        // if not overlapping then axis separates them
        if ( ! (
            std::get<1>(rec_points) > std::get<0>(box_points) &&
            std::get<1>(box_points) > std::get<0>(rec_points)
        )) separating_axis.push_back(axis);
    }

    return separating_axis;
}

std::vector<Vector2> GetSeparatingAxis(const Rectangle& rec1, const Rectangle& rec2)
{
    std::vector<Vector2> axies;
    std::vector<Vector2> separating_axis;
    axies.reserve(4);

    // both rectangles are aligned so we only have to check 2 axis
    axies.push_back({0, 1});
    axies.push_back({1, 0});

    // for each axis project the points of the two boxes to that axis
    // check if they collide on that 1d plane
    // if any plane has no collision then they are separated

    for (const auto& axis : axies)
    {
        std::tuple<float, float> rec1_points = GetMinAndMaxProjectedPoints(rec1, axis);
        std::tuple<float, float> rec2_points = GetMinAndMaxProjectedPoints(rec2, axis);

        // if not overlapping then axis separates them
        if ( ! (
            std::get<1>(rec1_points) > std::get<0>(rec2_points) &&
            std::get<1>(rec2_points) > std::get<0>(rec1_points)
        )) separating_axis.push_back(axis);
    }

    return separating_axis;
}

std::vector<Vector2> GetSeparatingAxis(const CollisionBox2D& box1, const CollisionBox2D& box2)
{
    std::vector<Vector2> axies;
    std::vector<Vector2> separating_axis;
    axies.reserve(4);

    // get the normals of the edges of the seed box
    axies.push_back(Vector2Normalize(Vector2Subtract(box1.top_left, box1.bottom_left)));
    axies.push_back(Vector2Normalize(Vector2Subtract(box1.top_right, box1.top_left)));
    axies.push_back(Vector2Normalize(Vector2Subtract(box2.top_left, box2.bottom_left)));
    axies.push_back(Vector2Normalize(Vector2Subtract(box2.top_right, box2.top_left)));

    // for each axis project the points of the two boxes to that axis
    // check if they collide on that 1d plane
    // if any plane has no collision then they are separated

    for (const auto& axis : axies)
    {
        std::tuple<float, float> box1_points = GetMinAndMaxProjectedPoints(box1, axis);
        std::tuple<float, float> box2_points = GetMinAndMaxProjectedPoints(box2, axis);

        // if not overlapping then axis separates them
        if ( ! (
            std::get<1>(box1_points) > std::get<0>(box2_points) &&
            std::get<1>(box2_points) > std::get<0>(box1_points)
        )) separating_axis.push_back(axis);
    }

    return separating_axis;
}

float GetMinProjectedPoint(const CollisionBox2D& box, Vector2 axis)
{
    return std::min(
        ProjectToAxis(box.bottom_left, axis),
        std::min(
            ProjectToAxis(box.top_left, axis),
            std::min(
                ProjectToAxis(box.top_right, axis),
                ProjectToAxis(box.bottom_right, axis)
            )
        )
    );
}

float GetMaxProjectedPoint(const CollisionBox2D& box, Vector2 axis)
{
    return std::max(
        ProjectToAxis(box.bottom_left, axis),
        std::max(
            ProjectToAxis(box.top_left, axis),
            std::max(
                ProjectToAxis(box.top_right, axis),
                ProjectToAxis(box.bottom_right, axis)
            )
        )
    );
}

float GetMinProjectedPoint(const Rectangle& rec, Vector2 axis)
{
    return std::min(
        ProjectToAxis({rec.x + rec.width, rec.y + rec.height}, axis),
        std::min(
            ProjectToAxis({rec.x + rec.width, rec.y}, axis),
            std::min(
                ProjectToAxis({rec.x, rec.y + rec.height}, axis),
                ProjectToAxis({rec.x, rec.y}, axis)
            )
        )
    );
}

float GetMaxProjectedPoint(const Rectangle& rec, Vector2 axis)
{
    return std::max(
        ProjectToAxis({rec.x + rec.width, rec.y + rec.height}, axis),
        std::max(
            ProjectToAxis({rec.x + rec.width, rec.y}, axis),
            std::max(
                ProjectToAxis({rec.x, rec.y + rec.height}, axis),
                ProjectToAxis({rec.x, rec.y}, axis)
            )
        )
    );
}

std::tuple<float, float> GetMinAndMaxProjectedPoints(const CollisionBox2D& box, const Vector2 axis)
{
    std::array<float, 4> points = {
        ProjectToAxis(box.top_left, axis),
        ProjectToAxis(box.top_right, axis),
        ProjectToAxis(box.bottom_left, axis),
        ProjectToAxis(box.bottom_right, axis)
    };

    // iterate over points to find min and max
    float min = points[0];
    float max = points[0];

    for (int i = 1; i < points.size(); i++)
    {
        if (points[i] < min)
            min = points[i];
        else if (points[i] > max)
            max = points[i];
    }

    return std::make_tuple(min, max);
}

std::tuple<float, float> GetMinAndMaxProjectedPoints(const Rectangle& rec, const Vector2 axis)
{
    std::array<float, 4> points = {
        ProjectToAxis({rec.x + rec.width, rec.y + rec.height}, axis),
        ProjectToAxis({rec.x + rec.width, rec.y}, axis),
        ProjectToAxis({rec.x, rec.y + rec.height}, axis),
        ProjectToAxis({rec.x, rec.y}, axis)
    };

    // iterate over points to find min and max
    float min = points[0];
    float max = points[0];

    for (int i = 1; i < points.size(); i++)
    {
        if (points[i] < min)
            min = points[i];
        else if (points[i] > max)
            max = points[i];
    }

    return std::make_tuple(min, max);
}
