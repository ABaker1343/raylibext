#include "../include/tilemap.hpp"

Tilemap::Tilemap(std::string filepath, Vector2 tile_size_texture, Vector2 map_size, Vector2 tile_size)
{
    // load the tilemap texture
    this->texture = LoadTexture(filepath.c_str());
    this->tile_size_texture.x = static_cast<int>(tile_size_texture.x);
    this->tile_size_texture.y = static_cast<int>(tile_size_texture.y);

    // create array of tiles
    int cols = this->texture.width / tile_size_texture.x;
    int rows = this->texture.height / tile_size_texture.y;
    this->max_tile_index = cols * rows;

    this->tile_size = tile_size;
    this->num_cols = static_cast<int>(map_size.x);
    this->num_rows = static_cast<int>(map_size.y);

    tiles.resize(map_size.y * map_size.y, Tile{0});
}

Tilemap::~Tilemap()
{
    UnloadTexture(texture);
}

Rectangle GetTileRect(const Tilemap& tilemap, int tile_index)
{
    Rectangle src;
    src.y = static_cast<int>(tile_index / tilemap.num_cols) * tilemap.tile_size_texture.y;
    src.x = (tile_index % tilemap.num_cols) * tilemap.tile_size_texture.x;
    src.width = tilemap.tile_size_texture.x;
    src.height = tilemap.tile_size_texture.y;

    return src;
}

void SetTile(Tilemap& tilemap, int x, int y, int tile_index)
{
    if (x > tilemap.num_cols || y > tilemap.num_rows)
    {
        return;
    }
    if (tile_index > tilemap.max_tile_index)
    {
        return;
    }

    int tile_to_set = x + (tilemap.num_cols * y);
    tilemap.tiles[tile_to_set].tile_index = tile_index;
}

int GetTileIndex(const Tilemap& tilemap, int x, int y)
{
    if (x > tilemap.num_cols)
    {
        throw std::runtime_error("GetTileIndex: x index too big");
    }
    if (y > tilemap.num_rows)
    {
        throw std::runtime_error("GetTileIndex: y index too big");
    }

    return x * (tilemap.num_cols * y);
}

void DrawTilemap(const Tilemap& tilemap, Vector2 position)
{
    Vector2 tile_position = position;

    for (const auto& tile : tilemap.tiles)
    {
        Rectangle src_rec = GetTileRect(tilemap, tile.tile_index);
        DrawTexturePro(
            tilemap.texture,
            src_rec,
            Rectangle{
                tile_position.x, tile_position.y,
                tilemap.tile_size.x, tilemap.tile_size.y
            },
            Vector2{0, 0},
            0,
            WHITE
        );

        tile_position.x += tilemap.tile_size.x;
        if (tile_position.x - position.x >= tilemap.num_cols * tilemap.tile_size.x)
        {
            tile_position.x = 0;
            tile_position.y += tilemap.tile_size.y;
        }
    }
}
