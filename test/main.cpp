#include <string>
#include <raylib.h>
#include <iostream>
#include <cstdlib>
#include "../include/raylibext.hpp"

int main()
{
    InitWindow(1920, 1080, "testing window");
    SetTargetFPS(60);

    CollisionBox2D box;
    box.bottom_left = {1920.f / 2 - 100, 1080.f / 2};
    box.bottom_right = {1920.f / 2 + 100, 1080.f / 2};
    box.top_left = {box.bottom_left.x, box.bottom_left.y + 500};
    box.top_right = {box.bottom_right.x, box.bottom_right.y + 500};

    Tilemap tilemap("./tilemap.png", {160, 90}, {10, 10}, {100, 100});
    std::cout << "max tile index: " << tilemap.max_tile_index << std::endl;;

    std::srand(std::time(NULL));
    for (auto& tile : tilemap.tiles)
    {
        tile.tile_index = std::rand() % tilemap.max_tile_index;
        std::cout << "assigned index: : " << tile.tile_index << std::endl;;
    }

    while (!WindowShouldClose())
    {
        Vector2 size = Vector2{8, 8};
        Vector2 midpoint = Vector2Midpoint(box.bottom_left, box.bottom_right);

        BeginDrawing();
            ClearBackground(BLACK);

            DrawTilemap(tilemap, Vector2{0, 0});

            DrawRectangleV(box.top_left, size,  RED);
            DrawRectangleV(box.top_right, size,  RED);
            DrawRectangleV(box.bottom_left, size,  RED);
            DrawRectangleV(box.bottom_right, size,  RED);
            DrawRectangleV(midpoint, size,  BLUE);

            std::string tl_string = "tl: " + std::to_string(box.top_left.x) + ", " + std::to_string(box.top_left.y);
            std::string tr_string = "tr: " + std::to_string(box.top_right.x) + ", " + std::to_string(box.top_right.y);
            std::string bl_string = "bl: " + std::to_string(box.bottom_left.x) + ", " + std::to_string(box.bottom_left.y);
            std::string br_string = "br: " + std::to_string(box.bottom_right.x) + ", " + std::to_string(box.bottom_right.y);

            int height = 32;
            DrawText(tl_string.c_str(), 0, height, 32, WHITE);
            height+=34;
            DrawText(tr_string.c_str(), 0, height, 32, WHITE);
            height+=34;
            DrawText(bl_string.c_str(), 0, height, 32, WHITE);
            height+=34;
            DrawText(br_string.c_str(), 0, height, 32, WHITE);

        EndDrawing();


        RotateCollisionBox(box, 1, BOTTOM_CENTER);
    }

    CloseWindow();
}
