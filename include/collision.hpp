#ifndef __HEADER_RAYLIBEXT_COLLISION
#define __HEADER_RAYLIBEXT_COLLISION

#include <optional>
#include <vector>
#include <array>
#include <raylib.h>
#include <raymath.h>

#include "operators.hpp"

struct CollisionBox2D
{
    Vector2 top_right;
    Vector2 top_left;
    Vector2 bottom_left;
    Vector2 bottom_right;

    CollisionBox2D();
    CollisionBox2D(const Vector2 bl, const Vector2 br, const Vector2 tl, const Vector2 tr);
    CollisionBox2D(const Rectangle rec);
};

enum CollisionBoxRotationPoint
{
    BOTTOM_CENTER,
    TOP_CENTER,
};

Vector2 Vector2Midpoint(const Vector2 v1, const Vector2 v2);
void RotateCollisionBox(CollisionBox2D& box, float degrees, CollisionBoxRotationPoint origin);

std::optional<Vector2> CheckCollisionLineRec(const Vector2& p1, const Vector2& p2, const Rectangle& rec);

bool CheckCollision(const CollisionBox2D& box, const Rectangle& rec);
bool CheckCollision(const CollisionBox2D& box1, const CollisionBox2D& box2);

std::vector<Vector2> GetSeparatingAxis(const CollisionBox2D& box, const Rectangle& rec);
std::vector<Vector2> GetSeparatingAxis(const Rectangle& rec1, const Rectangle& rec2);
std::vector<Vector2> GetSeparatingAxis(const CollisionBox2D& box1, const CollisionBox2D& box2);

float GetMinProjectedPoint(const CollisionBox2D& box, const Vector2 axis);
float GetMaxProjectedPoint(const CollisionBox2D& box, const Vector2 axis);

float GetMinProjectedPoint(const Rectangle& rec, const Vector2 axis);
float GetMaxProjectedPoint(const Rectangle& rec, const Vector2 axis);

std::tuple<float, float> GetMinAndMaxProjectedPoints(const CollisionBox2D& box, const Vector2 axis);
std::tuple<float, float> GetMinAndMaxProjectedPoints(const Rectangle& box, const Vector2 axis);

inline float ProjectToAxis(Vector2 point, Vector2 axis)
{
    return Vector2DotProduct(axis, point);
}

#endif
