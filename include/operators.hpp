#ifndef __HEADER_RAYLIBEXT_OPERATORS
#define __HEADER_RAYLIBEXT_OPERATORS

#include <raylib.h>
#include <raymath.h>

inline bool operator==(const Vector2& lhs, const Vector2& rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

inline bool operator!=(const Vector2& lhs, const Vector2& rhs)
{
    return !(lhs == rhs);
}

inline Vector2 operator+(const Vector2& lhs, const Vector2& rhs)
{
    return Vector2{lhs.x + rhs.x, lhs.y + rhs.y};
}

inline Vector2 operator-(const Vector2& lhs, const Vector2& rhs)
{
    return Vector2{lhs.x - rhs.x, lhs.y - rhs.y};
}

inline Vector2 operator*(const Vector2& lhs, const float& scale)
{
    return Vector2{lhs.x * scale, lhs.x * scale};
}

#endif
