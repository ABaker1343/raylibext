#ifndef __HEADER_RAYLIBEXT_TIMESTAMPS
#define __HEADER_RAYLIBEXT_TIMESTAMPS

#include <chrono>
typedef std::chrono::steady_clock GameClock;
typedef std::chrono::time_point<GameClock> GameTimePoint;

struct GameTimestamps
{
    GameTimePoint this_update;
    GameTimePoint last_update;
    std::chrono::milliseconds time_delta;
};

inline bool CheckCooldown(GameTimePoint time_now, GameTimePoint cooldown_start, std::chrono::milliseconds cooldown)
{
    if (std::chrono::duration_cast<std::chrono::milliseconds>(time_now - cooldown_start) >= cooldown)
        return true;
    
    return false;
}

inline void SetTimestampsThisUpdate(GameTimestamps& timestamps)
{
    timestamps.this_update = GameClock::now();
    timestamps.time_delta = std::chrono::duration_cast<std::chrono::milliseconds> (timestamps.this_update - timestamps.last_update);
}

inline void SetTimestampsLastUpdate(GameTimestamps& timestamps)
{
    timestamps.last_update = timestamps.this_update;
}

#endif
