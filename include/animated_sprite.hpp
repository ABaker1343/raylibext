#ifndef __HEADER_RAYLIBEXT_ANIMATED_SPRITE
#define __HEADER_RAYLIBEXT_ANIMATED_SPRITE

#include <chrono>
#include <string>
#include <raylib.h>
#include "timestamps.hpp"

struct AnimatedSprite
{
    Texture2D sprite;
    std::chrono::milliseconds animation_speed; // milliseconds per frame
    int num_frames;

    int current_frame;
    Rectangle frame_src;

    GameTimePoint timestamp_last_frame;

    AnimatedSprite(std::string filepath, int num_frames, float animation_fps);
    ~AnimatedSprite();
    AnimatedSprite(AnimatedSprite&) = delete; // no copy constructor
};

void UpdateAnimatedSprite(AnimatedSprite& sprite, GameTimePoint time_now);

#endif
