#ifndef __HEADER_RAYLIBEXT_TILEMAP
#define __HEADER_RAYLIBEXT_TILEMAP

#include <vector>
#include <string>
#include <stdexcept>
#include <raylib.h>


struct Tilemap
{
    struct Tile
    {
        int tile_index;
    };

    Texture2D texture;
    Vector2 tile_size_texture;
    int max_tile_index;

    std::vector<Tile> tiles;
    Vector2 tile_size;
    int num_rows;
    int num_cols;

    Tilemap(std::string filepath, Vector2 tile_size_texture, Vector2 map_size, Vector2 tile_size);
    ~Tilemap();
    Tilemap(Tilemap&) = delete;
};

Rectangle GetTileRect(const Tilemap& tilemap, int tile_index);
void SetTile(Tilemap& tilemap, int x, int y, int tile_index);
int GetTileIndex(const Tilemap& tilemap, int x, int y);

void DrawTilemap(const Tilemap& tilemap, Vector2 position);

#endif

